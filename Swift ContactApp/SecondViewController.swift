//
//  SecondViewController.swift
//  Swift ContactApp
//
//  Created by Francisco Morales on 10/14/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import UIKit
import RealmSwift
import MessageUI
import MapKit

class SecondViewController: UIViewController,UITextFieldDelegate, MFMailComposeViewControllerDelegate, MFMessageComposeViewControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    //PICKER VIEW FUNCTIONS
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = pickerData[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 10.0)!,NSForegroundColorAttributeName:UIColor.black])
        return myTitle
    }
    //DATA BEING PASSED FROM VIEW CONTROLLER
    var incomingContact: Contacts? = nil
    //REALM FOUNDATION
    let realm = try! Realm()
    //EMAIL RECIPIENT STRING ARRAY INITIATOR
    var emailReceipient = [String]()
    //PICKER DATA ARRAY
    let pickerData = ["Family","Friend","Coworker","School", "Other"]
    //IMAGE PICKER VARIABLE
    var imagePicker = UIImagePickerController()
    //OUTLETS
    @IBOutlet var firstNameTxtField: UITextField!
    @IBOutlet var lastNameTxtField: UITextField!
    @IBOutlet var phoneNumTxtField: UITextField!
    @IBOutlet var emailTxtField: UITextField!
    @IBOutlet var addressTxtField: UITextField!
    @IBOutlet var categoryTxtField: UITextField!
    @IBOutlet var categoryPicker: UIPickerView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var contactImageBtn: UIButton!
    @IBOutlet var mapView: MKMapView!
    
    //VIEW DID LOAD***********************************************************************
    override func viewDidLoad() {
        super.viewDidLoad()
        //IMAGE
        //self.imageView.image = imageToAdd
        //SETTING MAP REGION
        let distanceSpan:CLLocationDegrees = 3000
        let defaultLocation:CLLocationCoordinate2D = CLLocationCoordinate2DMake(41.1924454, -111.94187390000002)
        mapView.setRegion(MKCoordinateRegionMakeWithDistance(defaultLocation, distanceSpan, distanceSpan), animated: true)
        //CATEGORY PICKER STARTER
        categoryPicker.dataSource = self
        categoryPicker.delegate = self
        //LOADING INFORMATION TO TEXTFIELDS
        if (incomingContact != nil) {
            try! realm.write {
                firstNameTxtField.text = incomingContact?.fname
                lastNameTxtField.text = incomingContact?.lname
                phoneNumTxtField.text = incomingContact?.phoneNum
                emailTxtField.text = incomingContact?.email
                addressTxtField.text = incomingContact?.address
                categoryTxtField.text = incomingContact?.category
                //Append email address to send email to user choice
                emailReceipient.append(emailTxtField.text!)
            }
        }
        // Do any additional setup after loading the view.
        //ALLOWING TEXT FIELD DELEGATES TO DISMISS KEYBOARD
        self.firstNameTxtField.delegate = self
        self.lastNameTxtField.delegate = self
        self.phoneNumTxtField.delegate = self
        self.emailTxtField.delegate = self
        self.addressTxtField.delegate = self
        self.categoryTxtField.delegate = self
        
        //CREATING SAVE BUTTON FROM CODE
        let saveBtn = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.save, target: self, action: #selector(buttonTapped(_:)))
        //ADDING SAVE BUTTON TO NAVIGATION PANEL
        self.navigationItem.rightBarButtonItems = [saveBtn]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //SAVE BUTTON LOGIC
    @objc func buttonTapped (_ sender : UIButton) {
        print("save")
        //if let existingContact = incomingContact {
        if let existingContact = incomingContact {
            try! realm.write {
                existingContact.fname = firstNameTxtField.text!
                existingContact.lname = lastNameTxtField.text!
                existingContact.phoneNum = phoneNumTxtField.text!
                existingContact.email = emailTxtField.text!
                existingContact.address = addressTxtField.text!
                existingContact.category = categoryTxtField.text!
                
            }
        }
        else {
            let newContact = Contacts()
            newContact.fname = firstNameTxtField.text!
            newContact.lname = lastNameTxtField.text!
            newContact.phoneNum = phoneNumTxtField.text!
            newContact.email = emailTxtField.text!
            newContact.address = addressTxtField.text!
            newContact.category = categoryTxtField.text!
            
            try! realm.write {
                realm.add(newContact)
        }
        
        }
        navigationController?.popViewController(animated: true)

    }
    //CALL BUTTON
    @IBAction func CallPhoneButton(_ sender: Any) {
        
        if let url = URL(string: "tel://\(phoneNumTxtField.text!)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    //SEND TEXT BUTTON
    @IBAction func TextButton(_ sender: Any) {
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            //controller.body = "Message Body"
            controller.recipients = [phoneNumTxtField.text!]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
    }
    //SEND EMAIL BUTTON
    @IBAction func EmailButton(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail()
        {
            let composeMail = MFMailComposeViewController()
            composeMail.mailComposeDelegate = self
            composeMail.setToRecipients(emailReceipient)
            
            present(composeMail, animated: true)
        }
        else
        {
            let alertController = UIAlertController(title: "Email Error: Please check email settings.", message:
                "", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
        }
    }
    //IMAGE BUTTON
    @IBAction func contactImageBtnClicked(_ sender: Any) {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    //IMAGE PICKER + DISMISS IMAGE WHEN COMPLETE
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageView.image = image
        
        dismiss(animated: true, completion: nil)
        
    }
    //DISMISS SMS WHEN COMPLETE
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms screen actions
        self.dismiss(animated: true, completion: nil)
    }
    //DISMISS EMAIL WHEN COMPLETE
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    //HIDE KEYBOARD
    //Presses outside of keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    //Presses return key
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        firstNameTxtField.resignFirstResponder()
        lastNameTxtField.resignFirstResponder()
        phoneNumTxtField.resignFirstResponder()
        emailTxtField.resignFirstResponder()
        addressTxtField.resignFirstResponder()
        categoryTxtField.resignFirstResponder()
        
        return (true)
    }
    
}
