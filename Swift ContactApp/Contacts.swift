//
//  Contacts.swift
//  Swift ContactApp
//
//  Created by Francisco Morales on 10/14/17.
//  Copyright © 2017 Francisco Morales. All rights reserved.
//

import Foundation
import RealmSwift

class Contacts : Object {
    dynamic var name = ""
    dynamic var fname = ""
    dynamic var lname = ""
    dynamic var phone = 0
    dynamic var email = ""
    dynamic var address = ""
    dynamic var category = ""
    dynamic var phoneNum = ""
    //dynamic var picture : Data? = nil
}
